#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
#
# MIT License
#
# Copyright (c) 2018 Florent TOURNOIS
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# -----------------------------------------------------------------------------

import cmath 

# -----------------------------------------------------------------------------
def default_fun(value, constant):
    return value*value + constant

# -----------------------------------------------------------------------------
class MandelbrotSerie:
    # -------------------------------------------------------------------------
    def __init__(self, constant = None, fun = default_fun):
        self.__c = 0 
        if constant is not None:
            self.__c = constant

        self.__current_value = complex(0,0)
        self.__nb_iter = 0
        self.__fun = fun

    # -------------------------------------------------------------------------
    def next(self, step=1):
        while step>0:
            self.__current_value = self.__fun(self.__current_value, self.__c)
            self.__nb_iter += 1
            step-=1

    # -------------------------------------------------------------------------
    def square_module(self):
        if isinstance(self.__c, complex):
            return (self.__current_value.real)**2+\
                   (self.__current_value.imag)**2

        return self.__current_value**2

    # -------------------------------------------------------------------------
    @property
    def isdivergent(self):
        MAX=30

        while self.square_module()<4 and self.__nb_iter<MAX:
            self.next()

        return self.__nb_iter < MAX


    # -------------------------------------------------------------------------
    # __str__ is a built-in function that computes the "informal"
    # string reputation of an object
    # __str__ goal is to be readable
    # -------------------------------------------------------------------------
    def __str__(self):
        result = "Mandelbrot serie\n"
        result += "         value=%s\n" % self.__current_value
        result += "             n=%s\n" % self.__nb_iter
        result += "             c=%s\n" % self.__c
        return result


